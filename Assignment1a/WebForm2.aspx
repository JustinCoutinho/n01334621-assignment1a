﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="Assignment1a.WebForm2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Very Big Muscles Gym Club</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ValidationSummary ID="ValidSum" runat="server"/>
            <h1>Very Big Muscles Gym Club Registration</h1>
            <asp:TextBox runat="server" ID="Fname" placeholder="First Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your first name" ControlToValidate="Fname" ID="validatorFname"></asp:RequiredFieldValidator>
            <br />
            <asp:TextBox runat="server" ID="Lname" placeholder="Last Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your last name" ControlToValidate="Lname" ID="validatorLname"></asp:RequiredFieldValidator>
            <br />
            <asp:TextBox runat="server" ID="Phone" placeholder="Phone Number"></asp:TextBox>
            <asp:RegularExpressionValidator runat="server" ErrorMessage="Please enter a 10 digit number" ControlToValidate="Phone" ValidationExpression="^[0-9]{10}$"/>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your phone number" ControlToValidate="Phone" ID="validatorPhone"></asp:RequiredFieldValidator>
            <br />
            <asp:CompareValidator runat="server" ControlToValidate="Phone" Type="String" Operator="NotEqual" ValueToCompare="9051231234" ErrorMessage="This is not your phone number"></asp:CompareValidator>
            <br />
            <asp:TextBox runat="server" ID="customerEmail" placeholder="Email"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your Email" ControlToValidate="customerEmail" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="customerEmail" ErrorMessage="That's not an email!"></asp:RegularExpressionValidator>
            <br />
            <asp:TextBox runat="server" ID="mailingAddress" placeholder="Address"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="mailingAddress" ErrorMessage="Please enter your address"></asp:RequiredFieldValidator>
            <br />
            <h2>Gender</h2>
            <asp:RadioButton ID="radioM" runat="server" Text="Male" GroupName="gender"/>
            <asp:RadioButton ID="radioF" runat="server" Text="Female" GroupName="gender"/>
            <asp:RadioButton ID="radioO" runat="server" Text="Other" GroupName="gender"/>
            <h3>Membership Type</h3>
            <asp:DropDownList runat="server" ID="membershipType">
                <asp:ListItem Value="Basic" Text="Basic Muscles"></asp:ListItem>
                <asp:ListItem Value="Premium" Text="Premium Muscles"></asp:ListItem>
                <asp:ListItem Value="PremiumP" Text="Premium Plus Muscles"></asp:ListItem>
            </asp:DropDownList>
            <br />
            <h4>What are your favourite ways to train? (Select all that apply)</h4>
            <asp:CheckBox runat="server" ID="training1" Text="Cardio" />
            <asp:CheckBox runat="server" ID="training2" Text="Strength" />
            <asp:CheckBox runat="server" ID="training3" Text="Very Big Muscles" />
            <br />
            <h4>How did you hear about us? (Select all that apply)</h4>
            <asp:CheckBox runat="server" ID="option1" Text="Social Media" />
            <asp:CheckBox runat="server" ID="option2" Text="Website" />
            <asp:CheckBox runat="server" ID="option3" Text="Friend" />
            <asp:CheckBox runat="server" ID="option4" Text="An individual with very big muscles" />
            <asp:CheckBox runat="server" ID="option5" Text="Other" />
            <asp:TextBox runat="server" ID="Other" placeholder="Please share"></asp:TextBox>
            <br />
            <h5>Would you recommend us?</h5>
            <asp:RadioButton ID="RadioButton3" runat="server" Text="Y" GroupName="answer"/>
            <asp:RadioButton ID="RadioButton4" runat="server" Text="N" GroupName="answer"/>
            <asp:TextBox runat="server" ID="No" placeholder="Why not?"></asp:TextBox>
            <br />
            <asp:Button id="Button1" Text="Register" OnClick="SubmitBtn_Click" runat="server"/>
        </div>
    </form>
</body>
</html>

